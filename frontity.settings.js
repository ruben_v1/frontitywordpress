const settings = {
  "name": "wordpress-frontend",
  "state": {
    "frontity": {
      "url": "https://test.frontity.org",
      "title": "V1 Wordpress",
      "description": "WordPress installation for Frontity development"
    }
  },
  "packages": [
    {
      "name": "@frontity/mars-theme",
      "state": {
        "theme": {
          "menu": [],
          "featured": {
            "showOnList": false,
            "showOnPost": false
          }
        }
      }
    },
    {
      "name": "@frontity/wp-source",
      "state": {
        "source": {
          "url": "https://wordpress-backend.v1preview.nl/",
          "homepage": "home",
          "postsPage": "nieuws"
        }
      }
    },
    "@frontity/tiny-router",
    "@frontity/html2react",
    "@frontity/yoast",
    "@aamodtgroup/frontity-gravity-forms"
  ]
};

export default settings;
